import paddle
from utils.defEnhanceImg import *
# 定义数据读取类，继承Paddle.io.Dataset
class Dataset(paddle.io.Dataset):
    def  __init__(self, opt, mode='train'):
        if mode == "train":
            self.records = getLabelsAsRecords(opt,train=True)
        elif mode == "valid":
            self.records = getLabelsAsRecords(opt, train=False)
        else:
            print("TrainDataset:",mode," is not train or valid")
            exit(-1)
        self.img_size = opt.img_size  #get_img_size(mode)
    def __getitem__(self, idx):
        record = self.records[idx]
        img, gt_bbox, gt_labels, im_shape = get_img_data(record, size=self.img_size)

        return img, gt_bbox, gt_labels, np.array(im_shape)

    def __len__(self):
        return len(self.records)
if __name__ == "__main__":
    opt = getOptFromJson("../processFiles/opt.json")
    # 创建数据读取类
    train_dataset = Dataset(opt, mode='train')
    # 使用paddle.io.DataLoader创建数据读取器，并设置batchsize，进程数量num_workers等参数
    train_loader = paddle.io.DataLoader(train_dataset, batch_size=2, shuffle=True, num_workers=0, drop_last=True)
    img, gt_boxes, gt_labels, im_shape = next(train_loader())
    print(gt_labels)